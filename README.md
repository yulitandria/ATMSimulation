# ATMSimulation Study Case I
This repository was created in order to implement the requirement on 
https://github.com/Mitrais/java-bootcamp-working/wiki/ATM-Simulation


# Building Executable jar file
Assuming that the file pom.xml is already correct then you just need to execute this script on command prompt on your project directory

mvn package

# Running the App
After successfully built the jar, you can execute this script next

java -jar target\atmsimulation-0.0.1-SNAPSHOT.jar
