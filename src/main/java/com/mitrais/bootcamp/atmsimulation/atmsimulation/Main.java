package com.mitrais.bootcamp.atmsimulation.atmsimulation;

import com.mitrais.bootcamp.atmsimulation.screen.WelcomeScreen;

public class Main {

	public static void main(String[] args){
		/* Data set
		 * 
		Name : John Doe

		PIN : 012108

		Balance : $100

		Account Number: 112233

		Name : Jane Doe

		PIN : 932012

		Balance : $30

		Account Number: 112244 */
				
		while (true) {
			try {
				new WelcomeScreen().showScreen();
			} catch (Exception e) {
				System.err.println(e.getMessage());
			}
		}
	}
}
