package com.mitrais.bootcamp.atmsimulation.exception;

import com.mitrais.bootcamp.atmsimulation.screen.Screen;

public class GenericException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String message;

	//throw exception to Screen-extended classes
	public GenericException(Screen screen,String message) {
		this.message = message;
		System.err.println(this.message);
		try {
			screen.showScreen();
		} catch (GenericException e) {
		}
	}
	
	//throw exception and display error message
	public GenericException(String message) {
		new GenericException(true,message);
	}
	
	//throw exception, but displaying error message is optional
	public GenericException(boolean printMesssage,String message) {
		this.message = message;
		if(printMesssage) {
			System.err.println(this.message);
		}
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	
}
