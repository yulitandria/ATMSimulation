package com.mitrais.bootcamp.atmsimulation.pojo;

import java.math.BigDecimal;

public class FundTransfer {
	private Account originAccount;
	private Account destinatedAccount;
	private BigDecimal transferAmount;
	private Integer referenceNo;
	private boolean success;
	
	public Account getOriginAccount() {
		return originAccount;
	}
	public void setOriginAccount(Account originAccount) {
		this.originAccount = originAccount;
	}
	public Account getDestinatedAccount() {
		return destinatedAccount;
	}
	public void setDestinatedAccount(Account destinatedAccount) {
		this.destinatedAccount = destinatedAccount;
	}
	public BigDecimal getTransferAmount() {
		return transferAmount;
	}
	public void setTransferAmount(BigDecimal transferAmount) {
		this.transferAmount = transferAmount;
	}
	public Integer getReferenceNo() {
		return referenceNo;
	}
	public void setReferenceNo(Integer referenceNo) {
		this.referenceNo = referenceNo;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
}
