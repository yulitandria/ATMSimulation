package com.mitrais.bootcamp.atmsimulation.pojo;

public class FundTransferRequest {
	private Account originAccount;
	private String destinatedAccountNo;
	private String transferAmount;
	private String referenceNo;
	
	public Account getOriginAccount() {
		return originAccount;
	}
	public void setOriginAccount(Account originAccount) {
		this.originAccount = originAccount;
	}
	public String getDestinatedAccountNo() {
		return destinatedAccountNo;
	}
	public void setDestinatedAccountNo(String destinatedAccountNo) {
		this.destinatedAccountNo = destinatedAccountNo;
	}
	public String getTransferAmount() {
		return transferAmount;
	}
	public void setTransferAmount(String transferAmount) {
		this.transferAmount = transferAmount;
	}
	public String getReferenceNo() {
		return referenceNo;
	}
	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	
}
