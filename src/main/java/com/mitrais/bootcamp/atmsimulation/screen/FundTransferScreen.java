package com.mitrais.bootcamp.atmsimulation.screen;

import java.util.Scanner;

import com.mitrais.bootcamp.atmsimulation.exception.GenericException;
import com.mitrais.bootcamp.atmsimulation.pojo.Account;
import com.mitrais.bootcamp.atmsimulation.pojo.FundTransferRequest;
import com.mitrais.bootcamp.atmsimulation.util.GenerateUtil;

public class FundTransferScreen extends Screen {
	private Account account;
	
	public FundTransferScreen(Account account) {
		this.account = account;
	}
	

	@Override
	public void showScreen() throws GenericException {
		inputDestinatedAccount(account);
	}

	private void inputDestinatedAccount(Account account) throws GenericException {
		Scanner scan = new Scanner(System.in);
        System.out.println("________________________________________________\n");
        System.out.print("Please enter destination account and press enter to continue or \n" + 
        		"press (only) enter to go back to Transaction:");
        String destinationAccount = scan.nextLine();
        if(destinationAccount==null || destinationAccount.isEmpty()) {
        	new TransactionScreen(account).showScreen();
        }else {
        	inputTransferAmountScreen(account, scan, destinationAccount);
        }
	}

	private void inputTransferAmountScreen(Account account, Scanner scan, String destinationAccount) throws GenericException {
		System.out.println("________________________________________________\n");
		System.out.print("Please enter transfer amount and \n" + 
				"press enter to continue or \n" + 
				"press (only) enter to go back to Transaction: ");
		String transferAmount = scan.nextLine();
		if(transferAmount==null || transferAmount.isEmpty()) {
			new TransactionScreen(account).showScreen();
		}else {
			displayReferenceNoScreen(account, scan, destinationAccount, transferAmount);
		}
	}

	private void displayReferenceNoScreen(Account account, Scanner scan, String destinationAccount,
			String transferAmount) throws GenericException {
		int refNo = GenerateUtil.getRandomNumberBetween(999999, 100000);
		System.out.println("________________________________________________\n");
		System.out.print("Reference Number: "+refNo+"\r\n" + 
				"press enter to continue: ");
		String continueTrf = scan.nextLine();
		if(continueTrf==null || continueTrf.isEmpty()) {
			FundTransferRequest fundTransferRequest = new FundTransferRequest();
			fundTransferRequest.setDestinatedAccountNo(destinationAccount);
			fundTransferRequest.setOriginAccount(account);
			fundTransferRequest.setReferenceNo(String.valueOf(refNo));
			fundTransferRequest.setTransferAmount(transferAmount);
			new TransferConfirmationScreen(fundTransferRequest).showScreen();
		}
	}

}
