package com.mitrais.bootcamp.atmsimulation.screen;

import java.util.Arrays;
import java.util.Scanner;

import com.mitrais.bootcamp.atmsimulation.exception.GenericException;
import com.mitrais.bootcamp.atmsimulation.pojo.FundTransfer;
import com.mitrais.bootcamp.atmsimulation.util.GenerateUtil;

public class FundTransferSummaryScreen extends Screen{
	private FundTransfer fundTransfer;
	
	public FundTransferSummaryScreen(FundTransfer fundTransfer) {
		this.fundTransfer = fundTransfer;
	}


	@Override
	public void showScreen() throws GenericException {
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		printScreenTitle("Fund Transfer Summary");
		System.out.printf("Destination Account : %s%n" + 
				"Transfer Amount     : $%s%n" + 
				"Reference Number    : %s%n" + 
				"Balance    : $%s%n%n",
				fundTransfer.getDestinatedAccount().getAccountNumber(),
				fundTransfer.getTransferAmount(),
				fundTransfer.getReferenceNo(),
				fundTransfer.getOriginAccount().getBalance());
		printChoices(Arrays.asList("Transaction","Exit"));
		String choice = scan.nextLine();
		if(!GenerateUtil.validateStringNotEmptyAndMatchesRegex(choice, "[1-2]+")) {
		    throw new GenericException(new WelcomeScreen(), "Invalid input");
		}
		switch (choice) {
		case "1":
			new TransactionScreen(fundTransfer.getOriginAccount()).showScreen();
			break;		
		default:
			new WelcomeScreen().showScreen();
			break;
		}
	}
}
