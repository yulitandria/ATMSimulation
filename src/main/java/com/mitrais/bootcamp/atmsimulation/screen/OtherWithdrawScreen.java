package com.mitrais.bootcamp.atmsimulation.screen;

import java.util.Scanner;

import com.mitrais.bootcamp.atmsimulation.exception.GenericException;
import com.mitrais.bootcamp.atmsimulation.pojo.Account;
import com.mitrais.bootcamp.atmsimulation.services.AccountServiceImpl;
import com.mitrais.bootcamp.atmsimulation.util.GenerateUtil;

public class OtherWithdrawScreen extends Screen{
	private Account account;
	
	public OtherWithdrawScreen(Account account) {
		this.account = account;
	}

	@Override
	public void showScreen() throws GenericException  {
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
        System.out.println("________________________________________________\n");
        System.out.print("Other Withdraw\r\n" + 
        		"Enter amount to withdraw : ");
        String withdrawAmount = scan.nextLine();
        
        if (!GenerateUtil.validateStringNotEmptyAndMatchesRegex(withdrawAmount, "[0-9]+")) {
        	throw new GenericException(new OtherWithdrawScreen(account), "Invalid amount");
        }
        
        double amount = Double.parseDouble(withdrawAmount);
        
        try {
			GenerateUtil.validateAmountIsMultipleOf(amount,10);
			GenerateUtil.validateAmountIsNotGreaterthan(amount, 1000);
		} catch (GenericException e) {
			throw new GenericException(new OtherWithdrawScreen(account), e.getMessage());
		}
        
        account = new AccountServiceImpl().withdrawBalance(account, amount);
		new SummaryScreen(account, amount).showScreen();
	}

}
