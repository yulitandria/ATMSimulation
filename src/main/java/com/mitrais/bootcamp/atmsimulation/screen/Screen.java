package com.mitrais.bootcamp.atmsimulation.screen;

import java.util.List;

import com.mitrais.bootcamp.atmsimulation.exception.GenericException;

public abstract class Screen {

	//should be implemented on child class
	public abstract void showScreen() throws GenericException;
	
	//display choices
	public void printChoices(List<String> listChoices) {
		if(listChoices!=null && !listChoices.isEmpty()) {
			int counter = 1;
			for(String choice: listChoices) {
				System.out.println(counter+". "+choice);
				counter++;
			}
			System.out.print("Please choose option : ");
		}
	}
	
	//display screen title
	public void printScreenTitle(String title) {
		System.out.println("________________________________________________");
        System.out.println("\t\t"+title);
	}
		
}
