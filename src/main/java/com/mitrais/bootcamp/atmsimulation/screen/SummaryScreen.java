package com.mitrais.bootcamp.atmsimulation.screen;

import java.util.Arrays;
import java.util.Scanner;

import com.mitrais.bootcamp.atmsimulation.exception.GenericException;
import com.mitrais.bootcamp.atmsimulation.pojo.Account;
import com.mitrais.bootcamp.atmsimulation.util.GenerateUtil;

public class SummaryScreen extends Screen{
	private Account account;
	private double withdrawAmount;
	
	public SummaryScreen(Account account, double withdrawAmount) {
		this.account = account;
		this.withdrawAmount = withdrawAmount;
	}

	@Override
	public void showScreen() throws GenericException {
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
        printScreenTitle("Summary\n");
		System.out.printf("Date : %s%n", GenerateUtil.getTodayDateStringYYYMMDDHHMMa());
        System.out.printf("Withdraw : $%.0f%n",withdrawAmount);
        System.out.printf("Balance : $%.0f%n", account.getBalance());
        System.out.println("________________________________________________\n");
        printChoices(Arrays.asList("Transaction","Exit"));
        String choice = scan.nextLine();
        if(!GenerateUtil.validateStringNotEmptyAndMatchesRegex(choice, "[1-2]+")) {
	        throw new GenericException(new SummaryScreen(account,withdrawAmount), "Invalid input");
        }
        switch (choice) {
		case "1":
			new TransactionScreen(account).showScreen();;
			break;		
		default:
			new WelcomeScreen().showScreen();
			break;
		}
	}
}
