package com.mitrais.bootcamp.atmsimulation.screen;

import java.util.Arrays;
import java.util.Scanner;

import com.mitrais.bootcamp.atmsimulation.exception.GenericException;
import com.mitrais.bootcamp.atmsimulation.pojo.Account;
import com.mitrais.bootcamp.atmsimulation.util.GenerateUtil;

public class TransactionScreen extends Screen {
	private Account account;

	public TransactionScreen(Account account) {
		this.account = account;		
	}

	@Override
	public void showScreen() throws GenericException {
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		printScreenTitle("Your balance : $"+account.getBalance());
        System.out.println("________________________________________________\n");
        printChoices(Arrays.asList("Withdraw","Fund Transfer","Exit"));
        String choice = scan.nextLine();
        if(!GenerateUtil.validateStringNotEmptyAndMatchesRegex(choice, "[1-3]+")) {
	        throw new GenericException(new TransactionScreen(account),"Invalid input");
        }
        switch (choice) {
		case "1":
			new WithdrawScreen(account).showScreen();
			break;
		case "2":
			new FundTransferScreen(account).showScreen();
			break;

		default:
			new WelcomeScreen().showScreen();;
			break;
		}
		
	}
	
	

}
