package com.mitrais.bootcamp.atmsimulation.screen;

import java.util.Arrays;
import java.util.Scanner;

import com.mitrais.bootcamp.atmsimulation.exception.GenericException;
import com.mitrais.bootcamp.atmsimulation.pojo.FundTransfer;
import com.mitrais.bootcamp.atmsimulation.pojo.FundTransferRequest;
import com.mitrais.bootcamp.atmsimulation.services.AccountServiceImpl;
import com.mitrais.bootcamp.atmsimulation.util.GenerateUtil;

public class TransferConfirmationScreen extends Screen{
	private FundTransferRequest fundTransferRequest;
	
	
	public TransferConfirmationScreen(FundTransferRequest fundTransferRequest) {
		this.fundTransferRequest = fundTransferRequest;
	}


	@Override
	public void showScreen() throws GenericException {
		transferConfirmationScreen(fundTransferRequest);
	}
	
	private void transferConfirmationScreen(FundTransferRequest fundTransferRequest) throws GenericException {
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
        printScreenTitle("Transfer Confirmation");
		System.out.printf("Destination Account : %s%n" + 
				"Transfer Amount     : $%s%n" + 
				"Reference Number    : %s%n%n",
				fundTransferRequest.getDestinatedAccountNo(),
				fundTransferRequest.getTransferAmount(),
				fundTransferRequest.getReferenceNo());
		printChoices(Arrays.asList("Confirm Trx","Cancel Trx"));
		String choice = scan.nextLine();
		if(!GenerateUtil.validateStringNotEmptyAndMatchesRegex(choice, "[1-2]+")) {
		    throw new GenericException(new TransferConfirmationScreen(fundTransferRequest), "Invalid input");
		}
		switch (choice) {
		case "1":
			confirmTransfer(fundTransferRequest);
			break;		
		default:
			new TransactionScreen(fundTransferRequest.getOriginAccount()).showScreen();
			break;
		}
	}

	private void confirmTransfer(FundTransferRequest fundTransferRequest) throws GenericException {
		FundTransfer fundTransferResponse = new AccountServiceImpl().fundTransfer(fundTransferRequest);
		new FundTransferSummaryScreen(fundTransferResponse).showScreen();
	}

}
