package com.mitrais.bootcamp.atmsimulation.screen;

import java.util.Scanner;

import com.mitrais.bootcamp.atmsimulation.exception.GenericException;
import com.mitrais.bootcamp.atmsimulation.pojo.Account;
import com.mitrais.bootcamp.atmsimulation.services.AccountServiceImpl;

public class WelcomeScreen extends Screen{

	@Override
	public void showScreen() throws GenericException{
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
		printScreenTitle("Welcome Screen");
        System.out.print("Enter Account Number: ");
        String accNumber = scan.nextLine();
        System.out.print("Enter PIN: ");
        String pinNumber = scan.nextLine();
        
        Account account = new AccountServiceImpl().getAccountByAccNumberAndPin(accNumber, pinNumber);
		new TransactionScreen(account).showScreen();
        
	}
}
