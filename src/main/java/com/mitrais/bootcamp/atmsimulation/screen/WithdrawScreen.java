package com.mitrais.bootcamp.atmsimulation.screen;

import java.util.Arrays;
import java.util.Scanner;

import com.mitrais.bootcamp.atmsimulation.exception.GenericException;
import com.mitrais.bootcamp.atmsimulation.pojo.Account;
import com.mitrais.bootcamp.atmsimulation.services.AccountServiceImpl;
import com.mitrais.bootcamp.atmsimulation.util.GenerateUtil;

public class WithdrawScreen extends Screen{
	private Account account;
	
	public WithdrawScreen(Account account) {
		this.account = account;
	}
	
	@Override
	public void showScreen() throws GenericException {
		@SuppressWarnings("resource")
		Scanner scan = new Scanner(System.in);
        printScreenTitle("Withdraw options");
        printChoices(Arrays.asList("$10","$50","$100","Other","Back"));
        String choice = scan.nextLine();
        if(!GenerateUtil.validateStringNotEmptyAndMatchesRegex(choice, "[1-5]+")) {
	        throw new GenericException(new WithdrawScreen(account), "Invalid input");
        }
        switch (choice) {
		case "1":
			account = withdrawBalance(account, 10);
			new SummaryScreen(account, 10).showScreen();
			break;
		case "2":
			account = withdrawBalance(account, 50);
			new SummaryScreen(account, 50).showScreen();
			break;
		case "3":
			account = withdrawBalance(account, 100);
			new SummaryScreen(account, 100).showScreen();
			break;
		case "4":
			new OtherWithdrawScreen(account).showScreen();
			break;			
		default:
			new TransactionScreen(account).showScreen();;
			break;
		}
	}
	
	private Account withdrawBalance(Account account, double amount) throws GenericException {
		return new AccountServiceImpl().withdrawBalance(account, amount);
	}

	
}
