package com.mitrais.bootcamp.atmsimulation.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.mitrais.bootcamp.atmsimulation.exception.GenericException;
import com.mitrais.bootcamp.atmsimulation.pojo.Account;
import com.mitrais.bootcamp.atmsimulation.pojo.FundTransfer;
import com.mitrais.bootcamp.atmsimulation.pojo.FundTransferRequest;
import com.mitrais.bootcamp.atmsimulation.screen.FundTransferScreen;
import com.mitrais.bootcamp.atmsimulation.screen.WelcomeScreen;
import com.mitrais.bootcamp.atmsimulation.screen.WithdrawScreen;
import com.mitrais.bootcamp.atmsimulation.util.GenerateUtil;

public class AccountServiceImpl implements AccountServiceInterface{

	@Override
	public List<Account> getAllAccounts() {
		List<Account> listAccount = new ArrayList<Account>();
		listAccount.add(new Account("John Doe","012108",new BigDecimal(100),"112233"));
		listAccount.add(new Account("Jane Doe","932012",new BigDecimal(30),"112244"));
		listAccount.add(new Account("Jane Doe","123123",new BigDecimal(30),"123123"));
		return listAccount;
	}

	@Override
	public Account getAccountByAccNumberAndPin(String accNumber, String pinNumber) throws GenericException {
		if(!GenerateUtil.validateStringNotEmptyAndLength(accNumber,6)) {
        	throw new GenericException(new WelcomeScreen(),"Account Number should have 6 digits length");
        }
        
        if (!GenerateUtil.validateStringNotEmptyAndMatchesRegex(accNumber, "[0-9]+")) {
        	throw new GenericException(new WelcomeScreen(),"Account Number should only contains numbers");
        }
        
        if(!GenerateUtil.validateStringNotEmptyAndLength(pinNumber,6)) {
        	throw new GenericException(new WelcomeScreen(),"PIN should have 6 digits length");
        }
        
        if (!GenerateUtil.validateStringNotEmptyAndMatchesRegex(pinNumber, "[0-9]+")) {
        	throw new GenericException(new WelcomeScreen(),"PIN should only contains numbers");
        }
		Account filteredAccount = getAllAccounts().stream().filter(element-> accNumber.equalsIgnoreCase(element.getAccountNumber()) && pinNumber.equalsIgnoreCase(element.getPin())).findFirst().orElse(null);
		if(filteredAccount==null) {
        	throw new GenericException(new WelcomeScreen(),"Invalid Account Number/PIN");
        }
		return filteredAccount;
	}

	@Override
	public Account withdrawBalance(Account account, double amount) throws GenericException {
		BigDecimal deductionAmount = new BigDecimal(amount);
		BigDecimal finalBalance = account.getBalance().subtract(deductionAmount);
		if(GenerateUtil.biggerThanZero(finalBalance)) {
			throw new GenericException(new WithdrawScreen(account), "Insufficient balance $"+amount);
		}
		account.setBalance(finalBalance);		
		return account;
	}

	

	@Override
	public Account getAccountByAccNumber(String accNumber) throws GenericException {
		if(!GenerateUtil.validateStringNotEmptyAndLength(accNumber,6)) {
        	throw new GenericException(false, "Invalid Account");
        }
        
        if (!GenerateUtil.validateStringNotEmptyAndMatchesRegex(accNumber, "[0-9]+")) {
        	throw new GenericException(false, "Invalid Account");
        }
              
        Account filteredAccount = getAllAccounts().stream().filter(element-> accNumber.equalsIgnoreCase(element.getAccountNumber())).findFirst().orElse(null);
		
		if(filteredAccount==null) {
        	throw new GenericException(false, "Invalid Account");
        }
		return filteredAccount;
	}

	@Override
	public Account addBalance(Account account, double amount) {
		BigDecimal addAmount = new BigDecimal(amount);
		BigDecimal finalBalance = account.getBalance().add(addAmount);
		account.setBalance(finalBalance);		
		return account;
	}

	@Override
	public FundTransfer fundTransfer(FundTransferRequest fundTransferRequest) throws GenericException {
		//get destinated account
		Account destinatedAccount;
		try {
			destinatedAccount = getAccountByAccNumber(fundTransferRequest.getDestinatedAccountNo());
		} catch (GenericException e) {
			throw new GenericException(new FundTransferScreen(fundTransferRequest.getOriginAccount()), e.getMessage());
		}
		
		//validate referenceNo
		if (!GenerateUtil.validateStringNotEmptyAndMatchesRegex(fundTransferRequest.getReferenceNo(), "[0-9]+")) {
        	throw new GenericException(new FundTransferScreen(fundTransferRequest.getOriginAccount()), "Invalid Reference Number");
        }
		
		//subtract from origin account
		try {
			GenerateUtil.validateAmountToTransfer(fundTransferRequest.getTransferAmount());
		} catch (GenericException e) {
			throw new GenericException(new FundTransferScreen(fundTransferRequest.getOriginAccount()), e.getMessage());
		}
		double amount = Double.parseDouble(fundTransferRequest.getTransferAmount());
		Account originAccount = withdrawBalance(fundTransferRequest.getOriginAccount(),amount);
		
		//add balance to destinatedAccount
		destinatedAccount = addBalance(destinatedAccount, amount);
		FundTransfer fundTransfer = new FundTransfer();
		fundTransfer.setDestinatedAccount(destinatedAccount);
		fundTransfer.setOriginAccount(originAccount);
		Integer referenceNo = new Integer(fundTransferRequest.getReferenceNo());
		fundTransfer.setReferenceNo(referenceNo );
		fundTransfer.setSuccess(true);
		fundTransfer.setTransferAmount(new BigDecimal(amount));
		return fundTransfer;
	}
	
	
}
