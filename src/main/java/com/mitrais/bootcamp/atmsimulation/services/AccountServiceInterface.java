package com.mitrais.bootcamp.atmsimulation.services;

import java.util.List;

import com.mitrais.bootcamp.atmsimulation.exception.GenericException;
import com.mitrais.bootcamp.atmsimulation.pojo.Account;
import com.mitrais.bootcamp.atmsimulation.pojo.FundTransfer;
import com.mitrais.bootcamp.atmsimulation.pojo.FundTransferRequest;

public interface AccountServiceInterface {
	public List<Account> getAllAccounts();
	public Account getAccountByAccNumberAndPin(String accNumber, String pin) throws GenericException;
	public Account getAccountByAccNumber(String accNumber) throws GenericException;
	public Account withdrawBalance(Account account, double amount) throws GenericException;
	public Account addBalance(Account account, double amount);
	public FundTransfer fundTransfer(FundTransferRequest fundTransferRequest) throws GenericException;
}
