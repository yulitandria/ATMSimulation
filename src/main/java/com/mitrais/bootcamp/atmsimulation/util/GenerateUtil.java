package com.mitrais.bootcamp.atmsimulation.util;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

import com.mitrais.bootcamp.atmsimulation.exception.GenericException;

public class GenerateUtil {

	public static int getRandomNumberBetween(int max,int min) {
		Random r = new Random();
		return r.nextInt((max - min ) + 1) + min;
	}
	
	public static boolean biggerThanZero(BigDecimal finalBalance) {
		return finalBalance.compareTo(BigDecimal.ZERO)==-1;
	}
	
	public static boolean validateAmountToTransfer(String withdrawAmount) throws GenericException {
		if (!withdrawAmount.matches("[0-9]+")) {
        	throw new GenericException(false, "Invalid ammount");
        }
        
        double amount = Double.parseDouble(withdrawAmount);
        
        if(amount<1) {
        	throw new GenericException(false, "Minimum amount to transfer is $1");
        }
       
        if(amount>1000) {
        	throw new GenericException(false, "Maximum amount to transfer is $1000");
        }
        return true;
	}
	
	public static boolean validateAmountIsMultipleOf(double amount, double multipleOf) throws GenericException {
		if(amount%multipleOf!=0) {
        	throw new GenericException(false, "Invalid amount");
        }
		return true;
	}
	
	public static boolean validateAmountIsNotGreaterthan(double amount, double max) throws GenericException {
		if(amount>max) {
        	throw new GenericException(true, "Maximum amount to withdraw is $1000");
        }
		return true;
	}
	
	public static boolean validateStringNotEmptyAndMatchesRegex(String stringParameter, String regex) {
		if(stringParameter==null) {
		    return false;
		}
		if(stringParameter.isEmpty()) {
		    return false;
		}
		if (!stringParameter.matches(regex)) {
			return false;
	    }
		return true;
	}
	
	public static boolean validateStringNotEmptyAndLength(String stringParameter, int stringLength) {
		if(stringParameter==null) {
		    return false;
		}
		if(stringParameter.isEmpty()) {
		    return false;
		}
		if(stringParameter.length()!=stringLength) {
        	return false;
        }
		return true;
	}
	
	public static String getTodayDateStringYYYMMDDHHMMa() {
		LocalDateTime today = LocalDateTime.now();
        return getFormattedDateStringFromDate(today,"yyyy-MM-dd hh:mm a");
	}
	
	public static String getFormattedDateStringFromDate(LocalDateTime dateTime, String format) {
		DateTimeFormatter customFormatter = DateTimeFormatter.ofPattern(format);
        return dateTime.format(customFormatter);
	}
}
