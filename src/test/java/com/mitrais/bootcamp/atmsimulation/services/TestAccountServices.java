package com.mitrais.bootcamp.atmsimulation.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.List;

import org.junit.Test;

import com.mitrais.bootcamp.atmsimulation.exception.GenericException;
import com.mitrais.bootcamp.atmsimulation.pojo.Account;
import com.mitrais.bootcamp.atmsimulation.pojo.FundTransfer;
import com.mitrais.bootcamp.atmsimulation.pojo.FundTransferRequest;
import com.mitrais.bootcamp.atmsimulation.util.GenerateUtil;

public class TestAccountServices {
	
	@Test
	public void testgetAccountByAccNumberAndPin() throws GenericException {
		Account account = new Account("Jane Doe","932012",new BigDecimal(30),"112244");
		Account getAccount = new AccountServiceImpl().getAccountByAccNumberAndPin("112244", "932012");
		assertNotNull(getAccount);
		assertEquals(account.getAccountNumber(), getAccount.getAccountNumber());
		assertEquals(account.getPin(), getAccount.getPin());
		assertEquals(account.getBalance(), getAccount.getBalance());
		assertEquals(account.getName(), getAccount.getName());
	}
	
	@Test
	public void testgetAllAccount() {
		List<Account> getAccountAll = new AccountServiceImpl().getAllAccounts();
		assertNotNull(getAccountAll);
		assertEquals(3, getAccountAll.size());
	}
	
	@Test
	public void testwithdrawBalance() throws GenericException {
		Account account = new Account("Jane Doe","222222",new BigDecimal(100),"111111");
		
		Account subtractedAccount = new AccountServiceImpl().withdrawBalance(account, 30);
		assertNotNull(subtractedAccount);
		assertEquals(account.getAccountNumber(), subtractedAccount.getAccountNumber());
		assertEquals(new BigDecimal(70), subtractedAccount.getBalance());
	}
	
	@Test
	public void testgetAccountByAccoountNumber() throws GenericException {
		Account account = new Account("Jane Doe","123123",new BigDecimal(30),"123123");
		Account getAccount = new AccountServiceImpl().getAccountByAccNumber("123123");
		assertNotNull(getAccount);
		assertEquals(account.getAccountNumber(), getAccount.getAccountNumber());
		assertEquals(account.getPin(), getAccount.getPin());
		assertEquals(account.getBalance(), getAccount.getBalance());
		assertEquals(account.getName(), getAccount.getName());
	}
	
	@Test
	public void testAddBalance() {
		Account account = new Account("Jane Doe","222222",new BigDecimal(100),"111111");
		
		Account subtractedAccount = new AccountServiceImpl().addBalance(account, 30);
		assertNotNull(subtractedAccount);
		assertEquals(account.getAccountNumber(), subtractedAccount.getAccountNumber());
		assertEquals(new BigDecimal(130), subtractedAccount.getBalance());
	}

	@Test(expected = GenericException.class)
	public void testNegativeCaseAmountToTransfer() throws GenericException {
		GenerateUtil.validateAmountToTransfer("786jhgjh");
		GenerateUtil.validateAmountToTransfer("-1");
		GenerateUtil.validateAmountToTransfer("0");
		GenerateUtil.validateAmountToTransfer("1001");
	}
	
	@Test
	public void testvalidateAmountToTransfer() throws GenericException {
		assertTrue(GenerateUtil.validateAmountToTransfer("1000"));
		assertTrue(GenerateUtil.validateAmountToTransfer("500"));
	}
	
	@Test
	public void tesTransfer() throws GenericException {
		Account account = new Account("Jane Doe","222222",new BigDecimal(100),"111111");
		
		FundTransferRequest fundTransferRequest =  new FundTransferRequest();
		String destinatedAccNo = "112244"; //this account has $30 balance
		fundTransferRequest.setDestinatedAccountNo(destinatedAccNo);
		fundTransferRequest.setOriginAccount(account);
		fundTransferRequest.setReferenceNo("123456");
		fundTransferRequest.setTransferAmount("40");
		FundTransfer fundTransferResponse = new AccountServiceImpl().fundTransfer(fundTransferRequest );
		assertNotNull(fundTransferResponse);
		assertEquals(account.getAccountNumber(), fundTransferResponse.getOriginAccount().getAccountNumber());
		assertEquals(new BigDecimal(60), fundTransferResponse.getOriginAccount().getBalance());
		assertEquals(destinatedAccNo, fundTransferResponse.getDestinatedAccount().getAccountNumber());
		assertEquals(new BigDecimal(70), fundTransferResponse.getDestinatedAccount().getBalance());
	}
}
